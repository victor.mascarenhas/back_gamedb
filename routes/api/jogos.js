const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const Jogo = require('../../models/jogos');


//GET
router.get('/', async (req, res, next) => {
    try {
        const jogo = await Jogo.find({})
        res.json(jogo)        
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error":"Server error"})
    }
                
})

//GET REQ NOME
router.get('/:nomeJogo', async (req, res, next) => {
    try{        
        let param_nome = req.params["nomeJogo"]
        const jogo = await Jogo.findOne({nome : param_nome})
        if(jogo){
            res.json(jogo)
        }else{
            res.status(404).send({"error" : "jogo não encontrado"})
        }     
    }catch(err){
        console.error(err.message)
        res.status(500).send({'Error': 'Server error'})
    }
} )

//PUT
router.put('/:nomeJogo', [
    check('nome', "Name Required").not().isEmpty(),
    check('Dtlanc', "Dtlanc Required").not().isEmpty(),
    check('mode', "Mode Required").not().isEmpty(),
    check('plat', "Plat Required").not().isEmpty(),
    check('gen', "Gen Required").not().isEmpty(),
    check('dev', "Dev Required").not().isEmpty()
], async (req, res, next) => {
    try{
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            return res.status(400).json({ errors: errors.array()})
        }

        let param_nome = req.params ["nomeJogo"]
        let { nome, Dtlanc, mode, plat, gen, dev } = req.body
        const update = { nome, Dtlanc, mode, plat, gen, dev };
        let jogo = await Jogo.findOneAndUpdate({nome : param_nome}, update, {new : true})
        if(jogo){
            res.status(202).json(jogo)
        }else{
            res.status(404).send({"error": "jogo não encontrado"})
        }

    }catch(err){
        console.error(err.message)
        res.status(500).send({"error":"Server Error"})
    }
})

//PATCH
router.patch('/:nomeJogo', async (req, res, next) => {
    let param_nome = req.params ["nomeJogo"]
        try{
        const update = { $set: req.body }
        let jogo = await Jogo.findOneAndUpdate(param_nome, update, {new: true})               
        if (jogo){
            res.status(202).json(jogo);
        }else {
            res.status(404).send({ error: "Jogo não encontrado"})
        }      
      }
    catch(err){
        console.error(err.message)
        res.status(500).send({'error': 'server error'})
    }
})

//DELETE
router.delete('/:nomeJogo', async (req, res, next) => {
    let param_nome = req.params ["nomeJogo"]
        try{        
        let jogo = await Jogo.findOneAndDelete({ nome: param_nome })   
                   
        if (jogo){
            res.status(202).json(jogo);
        }else {
            res.status(404).send({ error: "Jogo não encontrado"})
        }      
      }
    catch(err){
        console.error(err.message)
        res.status(500).send({'error': 'server error'})
    }
})

//POST
router.post('/', [
    check('nome', "Name Required").not().isEmpty()
], async (req, res, next) => {
    try{
        let { nome, Dtlanc, mode, plat, gen, dev } = req.body        
        const errors = validationResult(req)
        console.error(errors)
        if (!errors.isEmpty()) {
            return res.status(400).json ({ errors: errors.array()})
        }else{
            let jogo = new Jogo({nome, Dtlanc, mode, plat, gen, dev})
        
        await jogo.save()        
        if (jogo.id){
            res.status(201).json(jogo);
        }      
      } 
    }catch(err){
        console.error(err.message)
        res.status(500).send({'error': 'server error'})
    }
})



module.exports = router;
