const express = require('express')
const app = express()
var bodyparser = require('body-parser')
const connectDB = require('./config/db')
var cors = require('cors')
const PORT = 4646;

//Init Middleware
app.use(cors())
app.use(express.json())
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());

//Connect Database
connectDB()

//Define Routes
app.use('/', require('./routes/hello'))
app.use('/jogo', require('./routes/api/jogos'))

app.listen(PORT, () => (console.log(`Rodando! Porta: ${PORT}`)))