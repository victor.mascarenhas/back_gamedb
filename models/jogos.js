const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    nome: {
        type : String,
        required : true,
        unique: true
    },
    Dtlanc: {
        type  : Date,
        required: true
    },
    mode: {
        type : String,
        required : true
    },
    plat: {
        type : String,
        required: true
    },
    gen: {
        type  : String,
        required: true
    },
    dev: {
        type  : String,
        required: true
    }
})

module.exports = mongoose.model('jogo', UserSchema);