 # Projeto GameDB

Cadastro de jogos eletrônicos.

## Versão

0.1.1 (beta)

## Pre requisitos

* Node

## Scripts Disponíveis
``` npm start ```

Inicia o aplicativo em modo de desenvolvimento na rota padrão http://localhost:4646

## Rotas Disponíveis

* /get: http://localhost:4646/jogo/
* /post: http://localhost:4646/jogo/

* /get: http://localhost:4646/jogo/NOMEDOJOGO
* /put: http://localhost:4646/jogo/NOMEDOJOGO
* /patch: http://localhost:4646/jogo/NOMEDOJOGO
* /delete: http://localhost:4646/jogo/NOMEDOJOGO
 
*substitua "NOMEDOJOGO" pelo nome do jogo que deseja incluir/modificar/acessar.

## Root API

routes/api/

## TO DO

* config deploy on Heroku